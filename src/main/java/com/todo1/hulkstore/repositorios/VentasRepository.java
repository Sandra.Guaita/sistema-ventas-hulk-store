package com.todo1.hulkstore.repositorios;

import org.springframework.data.repository.CrudRepository;
import com.todo1.hulkstore.modelo.Venta;
public interface VentasRepository extends CrudRepository<Venta, Integer> {
}
