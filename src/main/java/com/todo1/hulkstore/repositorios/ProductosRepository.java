package com.todo1.hulkstore.repositorios;

import org.springframework.data.repository.CrudRepository;
import com.todo1.hulkstore.modelo.*;
public interface ProductosRepository extends CrudRepository<Producto, Integer> {

    Producto findFirstByCodigo(String codigo);
}
