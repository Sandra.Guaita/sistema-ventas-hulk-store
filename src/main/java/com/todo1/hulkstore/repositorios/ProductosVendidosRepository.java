package com.todo1.hulkstore.repositorios;

import org.springframework.data.repository.CrudRepository;
import com.todo1.hulkstore.modelo.ProductoVendido;

public interface ProductosVendidosRepository extends CrudRepository<ProductoVendido, Integer> {

}
