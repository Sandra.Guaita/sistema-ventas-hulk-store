# Sistema-ventas-Hulk Store
https://gitlab.com/Sandra.Guaita/sistema-ventas-hulk-store.git

## Descargar código del sistema de ventas Spring Boot y compilar

ejecuta el programa con:

`gradlew bootRun`

Y crea el jar usando:

`gradlew build`

No olvides que dejo el código fuente en [GitHub]().

## application.properties para el sistema de ventas HulkStore

modificar el aplication properties

## Esquema de base de datos
No es necesario proporcionar el esquema de la base de datos pues la migración se hace automáticamente al ejecutar el proyecto; sin embargo
se puede encontrar un esquema en src/main/resources/


##url 
http://localhost:8080/ventas/
